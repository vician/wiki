## Networking

### Manual

	ip addr add 192.168.0.2/24 dev eth0
	ip route add devault via 192.168.0.1

### Static

Create or edit a file `/etc/sysconfig/network-scripts/ifcfg-eth0` with content:

	DEVICE=eth0
	IPV6INIT=no
	IPV6_AUTOCONF=no
	ONBOOT=yes
	IPADDR=192.168.0.2
	NETMASK=255.255.255.0
	GATEWAY=192.168.0.1
	NM_CONTROLLED=no
