## Enable IPv6 for docker daemon

Fill this to your `/etc/docker/daemon.json`:
```
{
	"ipv6": true,
	"fixed-cidr-v6": "fd00:ffff::/80",
	"ip6tables": true,
	"experimental": true
}
```
You can choose your own prefix instead of `fd00:ffff::/80` but `/80` is recommended as minimum.

And restart the docker daemon:
```
systemctl restart docker
```
And test it:
```
docker run --rm -t busybox ping6 -c 4 google.com
```

## Enable IPv6 in your docker-compose

Add to your `docker-compose.yml`:
```
version: '2.4'

services:
  servicename:
  ...
    networks:
      - servicenetwork
networks:
  servicenetwork:
    name: servicenetwork
    enable_ipv6: true
    ipam:
      config:
        - subnet: fd00:a::/80
```

The subnet needs to be different from the one used in docker daemon configuration.

```
docker-compose up -d
```
