## Automatic Attach of SSH session

	if [[ "$TMUX" == "" ]] &&
			[[ "$SSH_CONNECTION" != "" ]]; then
		WHOAMI=$(whoami)
		if tmux has-session -t $WHOAMI 2>/dev/null; then
			tmux -2 attach-session -t $WHOAMI
		else
			tmux -2 new-session -s $WHOAMI
		fi
	fi

## Fix SSH agent when attaching to existing tmux session

[source](https://stackoverflow.com/questions/21378569/how-to-auto-update-ssh-agent-environment-variables-when-attaching-to-existing-tm)

- Create file `~/.ssh/rc` with content:

		#!/bin/bash

		# Fix SSH auth socket location so agent forwarding works with tmux
		if test "$SSH_AUTH_SOCK" ; then
		  ln -sf $SSH_AUTH_SOCK $HOME/.ssh/ssh_auth_sock
		fi

- Make it executable

		chmod u+x ~/.ssh/rc

- Specify SSH auth socket by adding this to your `~/.bashrc`:

		export SSH_AUTH_SOCK=$HOME/.ssh/ssh_auth_sock
