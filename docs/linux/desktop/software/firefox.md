## Dark input files on Gnome

[source](https://www.dovydasvenckus.com/linux/2018/08/20/fix-firefox-dark-input-fields-on-gnome/)

1. Go to `about:config`
2. Create a new string (right click on top bar with fields name) `widget.content.gtk-theme-override`
3. And set it to `Arc-Darker` or `Adwaita:light`

## Two Firefox profiles with different icons

1. Create two profiles (one as a default profile), you can use:
	- Go to `about:profiles` in Firefox.
	- Run `firefox --ProfileManager --no-remote` from terminal or `ALT+F2`
1. Remember names of your profiles (e.g. `personal` and `business` - I assume profile `business` is a default profile - it means that hyperlinks outside Firefox browser will open in this profile)
1. Create a personal desktop shortcut with different names and icons
	- Find an original Firefox desktop shortcut file: `locate firefox.desktop` - it should be something like `/usr/share/applications/firefox.desktop`
	- Create your personal profile desktop file: `cp /usr/share/applications/firefox.desktop ~/.local/share/applications/firefox-personal.desktop`
	- Change line `Name=Firefox` to something like `Name=Firefox Personal`
	- Change all lines with `Exec=firefox ...` to `Exec=firefox --class=firefox-personal --no-remote -P personal ...`
	- Change line `Icon=firefox` to something like:
		- `Icon=firefox-trunk-alt`
		- or any other icons from your icon theme `find /usr/share/icons/ -iname "firefox*"`
1. Business profile will be the default profile with default icon, name, etc.
1. Now you should see a two icons in your Gnome3 menu and two icons in your `ALT+TAB` and Dash panel (if you have it).
