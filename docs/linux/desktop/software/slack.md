## Fix wrong timezone

	cp /var/lib/flatpak/app/com.slack.Slack/x86_64/stable/active/export/share/applications/com.slack.Slack.desktop ~/.local/share/applications/
	sed -i "s/run/run --env=TZ=$(timedatectl | grep "Time zone" | awk '{print $3}' | sed 's/\//\\\//g')/g" ~/.local/share/applications/com.slack.Slack.desktop
