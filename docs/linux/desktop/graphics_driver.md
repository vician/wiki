[Install Nvidia on Fedora 29](https://wiki.vician.dev/linux/desktop/hardware/Laptop_Lenovo_T480/#install-nvidia)

## Get current status

### Current Drivers in use

	lspci -k | grep -EA3 'VGA|3D|Display'

### Current Drivers for 3D

	glxinfo | grep OpenGL

### Xorg or Wayland?

	loginctl show-session $(loginctl | grep $USER | awk '{print $1}') -p Type

## Install Nvidia

### Fedora Workstation

[Source](https://fedoramagazine.org/install-nvidia-gpu/)

	sudo dnf update
	reboot # Will reboot your computer - do the next steps after it
	sudo dnf install fedora-workstation-repositories
	sudo dnf config-manager --set-enabled rpmfusion-nonfree-nvidia-driver
	reboot # Will reboot your computer - do the next steps after it
	sudo dnf repository-packages rpmfusion-nonfree-nvidia-driver info

- Login, connect to the internet, and open the Software app. Click Add-ons> Hardware Drivers> NVIDIA Linux Graphics Driver> Install.
- Reboot
- Verify that secure boot is disabled

### Fedora Silverblue

	rpm-ostree install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-30.noarch.rpm https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-30.noarch.rpm
	systemctl reboot
	rpm-ostree install kmod-nvidia xorg-x11-drv-nvidia
	rpm-ostree kargs --append=rd.driver.blacklist=nouveau --append=modprobe.blacklist=nouveau --append=nvidia-drm.modeset=0 --append=nouveau.modeset=0
	systemctl reboot

## Enable Nvidia as a primary driver

[Source](https://ask.fedoraproject.org/t/optimus-setting-the-nvidia-gpu-as-primary-rpmfusion-in-fedora-32-workstation/6550)

	sudo cp -p /usr/share/X11/xorg.conf.d/nvidia.conf /etc/X11/xorg.conf.d/nvidia.conf
	# add Option "PrimaryGPU" "yes" to both sections

And reboot

## Test 3D rendering

	glxgears -info

## Block Nvidia while booting

Add `modprobe.blacklist=nouveau,nvidia,nvidia_uvm,nvidia_drm,nvidia_modeset`  to end of `GRUB_CMDLINE_LINUX="..."` in `/etc/sysconfig/grub` and [update grub2](#grub2-update)

Or on Silverblue:

	rpm-ostree kargs --append=modprobe.blacklist=nouveau,nvidia,nvidia_uvm,nvidia_drm,nvidia_modeset

## i915 blinking

Add `i915.enable_psr=0`  to end of `GRUB_CMDLINE_LINUX="..."` in `/etc/sysconfig/grub` and [update grub2](#grub2-update)


Or on Silverblue:

	rpm-ostree kargs --append=i915.enable_psr=0

## Grub2 update

_Only on Fedora Workstation - not on Silverblue._

	# BIOS
	grub2-mkconfig -o /boot/grub2/grub.cfg
	# UEFI
	grub2-mkconfig -o /boot/efi/EFI/fedora/grub.cfg
