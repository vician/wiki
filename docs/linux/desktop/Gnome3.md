## Gnome Tweak Tool

Install package `gnome-tweak-tool`:

- Ubuntu:

		sudo apt install gnome-tweak-tool

- Fedora:

		sudo dnf install gnome-tweak-tool

## Theme

- Install gnome-extension support:
	- Ubuntu:

			sudo apt install chrome-gnome-shell

	- Fedora:

			sudo dnf install chrome-gnome-shell

- Go to [user themes extension](https://extensions.gnome.org/extension/19/user-themes/) install required browser extension and the extension itself

### Arc theme and Papirus icon theme

- Ubuntu:

		sudo apt install arc-theme papirus-icon-theme

- Fedora:

		sudo dnf install arc-theme papirus-icon-theme

- change the appearence in gnome tweak tool

## Gnome Shell Extensions

- [AlternateTab](https://extensions.gnome.org/extension/15/alternatetab/) - Substitute Alt-Tab
- [Caffeine](https://extensions.gnome.org/extension/517/caffeine/) - Disable the screensaver and auto suspend
- [Dash to Dock](https://extensions.gnome.org/extension/307/dash-to-dock/) - A dock for the Gnome Shell
- [Freon](https://extensions.gnome.org/extension/841/freon/) - Shows CPU temperature, disk temperature, video card temperature
- [Notification Counter](https://extensions.gnome.org/extension/1386/notification-counter/) - Shows number of notifications in queue
- [Openweather](https://extensions.gnome.org/extension/750/openweather/) - Weather extension to display weather information
- [ShellTile](https://extensions.gnome.org/extension/657/shelltile/) - A tiling window extension
- [Sound Input & Output Device Chooser](https://extensions.gnome.org/extension/906/sound-output-device-chooser/) - Shows a list of sound output and input devices in the status menu
- [Suspend Button](https://extensions.gnome.org/extension/826/suspend-button/) - Add missing suspend button
- [System Monitor](https://extensions.gnome.org/extension/120/system-monitor/) - Display system informations
- [Toggle Touchscreen](https://extensions.gnome.org/extension/991/toggle-touchscreen/) - Switch touchscreen on and off
- [TopIcons Plus](https://extensions.gnome.org/extension/1031/topicons/) - Enables legacy tray icons
- [User Themes](https://extensions.gnome.org/extension/19/user-themes/) - Load shell themes from user directory
