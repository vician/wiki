## Disable Infrared Webcam

All IDs are for Lenovo ThinkPad T480.

### Manually

```
echo 0 | sudo tee /sys/bus/usb/devices/1-5/bConfigurationValue
```

### Automatically

Create a file `/etc/udev/rules.d/90-disable-infrared-webcam.rules` with content:
```
ACTION!="add|change", GOTO="camera_end"

ATTRS{idVendor}=="5986",ATTRS{idProduct}=="1141",ATTR{bConfigurationValue}="0"

LABEL="camera_end"
```
