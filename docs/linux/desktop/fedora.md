## Use GPG2 as default

	old_gpg=$(which gpg)
	sudo mv $old_gpg ${old_gpg}1
	sudo ln -s $(which gpg2) $old_gpg

## DNSSEC

[Source](https://fedoraproject.org/wiki/Networking/NameResolution/DNSSEC)

	sudo dnf install dnssec-trigger
	sudo systemctl start dnssec-triggerd

Your `/etc/resolv.conf` will contain only local resolver `127.0.0.1`. You can find the real resolvers in `/var/run/NetworkManager/resolv.conf`


## Knot resolver

- Install and enable knot resolver

		sudo dnf install knot-resolver
		sudo systemctl enable kresd@1.service
		sudo systemctl start kresd@1.service

- Get back control over `/etc/resolv.conf`

		echo -e "[main]\ndns=none" | sudo tee /etc/NetworkManager/conf.d/no-dns.conf
		sudo rm /etc/resolv.conf
		echo -e "search yourexample.com\nnameserver 127.0.0.1" | sudo tee /etc/resolv.conf
		sudo systemctl restart NetworkManager.service

### Internal zones

If you have some internal DNS zones you can add them by adding these lines to your `/etc/knot-resolver/kresd.conf`:

	internaldomains = policy.todnames({'example1.com','example2.com'})
	policy.add(policy.suffix(policy.FLAGS({'NO_CACHE'}), internaldomains))
	policy.add(policy.suffix(policy.STUB('192.168.0.1','.192.168.0.2'), internaldomains))
	policy.add(policy.suffix(policy.STUB('192.168.0.3'), {todname('example3.com')}))

### Revert

		sudo rm /etc/NetworkManager/conf.d/no-dns.conf
		sudo ln -s /var/run/NetworkManager/resolv.conf /etc/resolv.conf

## Bluetooth codecs

[Eischmann's article](https://eischmann.wordpress.com/2019/02/11/better-bluetooth-sound-quality-on-linux/), [Pulseaudio's issue](https://github.com/EHfive/pulseaudio-modules-bt/issues/20)

	sudo dnf install pulseaudio-module-bluetooth-freeworld --allowerasing
	pulseaudio -k

## Constant reawakening after suspending

[Source](https://www.reddit.com/r/Fedora/comments/7ya01j/systemd_unit_to_prevent_occasional/) _Works on SilverBlue as well._

	sudo vi /etc/systemd/system/suppress-rogue-acpi-wakeups.service

With content:

	[Unit]
	Description=Suppress rogue ACPI wakeups

	[Service]
	Type=oneshot
	ExecStart=/bin/sh -c "egrep -q XHCI.+enabled /proc/acpi/wakeup && echo XHCI > /proc/acpi/wakeup; egrep -q EHC1.+enabled /proc/acpi/wakeup && echo EHC1 > /proc/acpi/wakeup; egrep -q XHC.+enabled /proc/acpi/wakeup && echo XHC > /proc/acpi/wakeup"


	[Install]
	WantedBy=multi-user.target

And run:

	sudo chmod 755 /etc/systemd/system/suppress-rogue-acpi-wakeups.service

	sudo systemctl enable suppress-rogue-acpi-wakeups.service

Another possibility is to use `/lib/systemd/system-sleep` which doesn't work on SilverBlue because it's read-only partition there. [Source](https://bugs.launchpad.net/ubuntu/+source/linux/+bug/1774994)
