## Install additional packages

	rpm-ostree install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-32.noarch.rpm https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-32.noarch.rpm
	rpm-ostree install https://dl.google.com/linux/direct/google-chrome-stable_current_x86_64.rpm
	rpm-ostree install https://mega.nz/linux/MEGAsync/Fedora_32/x86_64/megasync-Fedora_32.x86_64.rpm https://mega.nz/linux/MEGAsync/Fedora_32/x86_64/nautilus-megasync-Fedora_32.x86_64.rpm

## Automatic Updates

[Source](https://miabbott.github.io/2018/06/13/rpm-ostree-automatic-updates.html)

	sudo vi /etc/rpm-ostreed.conf

Set `AutomaticUpdatePolicy` to `stage`.

	systemctl reload rpm-ostreed
	systemctl enable rpm-ostreed-automatic.timer --now

## Nvidia

[Source](https://blogs.gnome.org/alexl/2019/03/06/nvidia-drivers-in-fedora-silverblue/)

_Tested on Fedora 30._

	rpm-ostree install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-32.noarch.rpm https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-32.noarch.rpm
	systemctl reboot
	rpm-ostree install kmod-nvidia xorg-x11-drv-nvidia
	rpm-ostree kargs --append=rd.driver.blacklist=nouveau --append=modprobe.blacklist=nouveau --append=nvidia-drm.modeset=0 --append=nouveau.modeset=0
	systemctl reboot


## Update Layered repositories

	rpm-ostree uninstall $(rpm-ostree status | grep -w " google-chrome" 2>/dev/null 3>/dev/null | sed 's/LocalPackages://g' | sed 's/ //g' | sort -nr | head -n 1) && rpm-ostree install https://dl.google.com/linux/direct/google-chrome-stable_current_x86_64.rpm
	rpm-ostree uninstall $(rpm-ostree status | grep -w " megasync" 2>/dev/null 3>/dev/null | sed 's/LocalPackages://g' | sed 's/ //g' | sort -nr | head -n 1) $(rpm-ostree status | grep -w " nautilus-megasync" 2>/dev/null 3>/dev/null | sed 's/LocalPackages://g' | sed 's/ //g' | sort -nr | head -n 1)&& rpm-ostree install https://mega.nz/linux/MEGAsync/Fedora_32/x86_64/megasync-Fedora_32.x86_64.rpm https://mega.nz/linux/MEGAsync/Fedora_32/x86_64/nautilus-megasync-Fedora_32.x86_64.rpm
