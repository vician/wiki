## List of cotainers

	podman ps

## Create new CentOS container

	podman pull centos
	podman run -it --rm docker.io/library/centos /bin/bash
	# Or with shared path
	podman run -itv /home/user/Documents/otherpath:/host:ro,z --rm docker.io/library/centos /bin/bash
