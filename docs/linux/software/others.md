## Change tag of audio files

[puddletag](http://docs.puddletag.net/)

- Ubuntu:

		sudo apt instal puddletag

- Fedora:

		sudo dnf instal puddletag

## Extract pdf to png - one image per file

	convert -density 300 file.pdf page_%04d.jpg
