
<form action="javascript:pace2speed();">
Pace: <input type="text" id="min" value="5" size="2"> min <input id="sec" type="text" value="30" size="2"> sec
<input type="submit">
</form>

Speed: <input type="text" id="mps" disabled>m/s<br/>
Speed: <input type="text" id="kmph" disabled>km/h<br/>
Speed: <input type="text" id="mph" disabled>miles/h

<script>
function pace2speed() {
  var min = parseInt($("#min").val());
  var sec = parseInt($("#sec").val());

  min = min + sec/60;

  var hour = min/60;

  sec = min*60 + sec;
  var mps = 1/sec*1000;
  $("#mps").val(mps.toFixed(2));

  var kmph = 1/hour;
  $("#kmph").val(kmph.toFixed(2));

  var mph = kmph/1.609344;
  $("#mph").val(mph.toFixed(2));


  }
</script>
