# Introduction

- [My website in English](https://vician.net)
- [My website in Czech](https://vician.cz)

## Wiki building status

[![](https://gitlab.com/vician/wiki/badges/master/pipeline.svg)](https://gitlab.com/vician/wiki)
